module gitlab.com/go-cmds/gowgkeygen

go 1.17

require (
	github.com/spf13/pflag v1.0.5
	golang.org/x/crypto v0.6.0
)
